package com.vkluchak.timezonemvvm.presentation.main.model

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.ReplayRelay
import com.vkluchak.timezonemvvm.core.TimeZoneService
import com.vkluchak.timezonemvvm.infrastracture.ArchCompViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class MainViewModel : ArchCompViewModel() {

    abstract val errorReplayRelay: ReplayRelay<String>
    abstract val timeZoneListRelay: PublishRelay<List<String>>

    abstract fun getTimeZoneList()
}

class MainViewModelImpl(private val timeZoneService: TimeZoneService) : MainViewModel() {

    override val errorReplayRelay: ReplayRelay<String> = ReplayRelay.create()
    override val timeZoneListRelay: PublishRelay<List<String>> = PublishRelay.create()

    override fun getTimeZoneList() {
        timeZoneService.getTimeZoneNamesList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { errorReplayRelay.accept(it.message) }
            .doOnSuccess { timeZoneListRelay.accept(it) }
            .subscribe()
    }

}