package com.vkluchak.timezonemvvm.presentation.utils.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Build
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import com.vkluchak.timezonemvvm.R
import org.joda.time.DateTime
import org.joda.time.DateTimeZone


class CustomClockView : View {

    private var height: Float = 0.0f
    private var width = 0.0f
    private var padding = 0
    private var fontSize = 0
    private val numeralSpacing = 0
    private var handTruncation: Int = 0
    private var hourHandTruncation = 0
    private var radius = 0
    private var paint: Paint? = null
    private var isInit: Boolean = false
    private val numbers = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)
    private val rect = Rect()

    @ColorInt
    var clockColor: Int = Color.RED
    var timeZoneName: String? = null

    @SuppressLint("CustomViewStyleable")
    private fun init(@Nullable set: AttributeSet?) {

        val typedArray = context.obtainStyledAttributes(set, R.styleable.CustomClockViewAttr, 0, 0)
        try {
            clockColor = typedArray.getColor(R.styleable.CustomClockViewAttr_color, Color.GREEN)
            timeZoneName = typedArray.getString(R.styleable.CustomClockViewAttr_time_zone)
        } finally {
            // release the TypedArray so that it can be reused.
            typedArray.recycle()
        }

    }

    private fun initClock() {
        paint = Paint()

        height = getHeight().toFloat()
        width = getWidth().toFloat()
        padding = numeralSpacing + 50
        fontSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP, 13f,
            resources.displayMetrics
        ).toInt()
        val min = Math.min(height, width)
        radius = (min / 2 - padding).toInt()
        handTruncation = (min / 20).toInt()
        hourHandTruncation = (min / 7).toInt()

        isInit = true
    }

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, @Nullable attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
        init(attrs)
    }

    override fun onDraw(canvas: Canvas) {
        if (!isInit) {
            initClock()
        }

        initPaint()
        drawCircle(canvas)
        drawCenter(canvas)
        drawNumeral(canvas)
        drawHands(canvas)

        postInvalidateDelayed(500)
        invalidate()
    }

    private fun initPaint(){
        paint!!.reset()
        paint?.color = clockColor
        paint!!.strokeWidth = 8F
        paint!!.style = Paint.Style.STROKE
        paint!!.isAntiAlias = true
    }

    private fun drawCircle(canvas: Canvas) {
        canvas.drawCircle(width / 2, height / 2, (radius + padding - 10).toFloat(), paint!!)
    }

    private fun drawCenter(canvas: Canvas) {
        paint!!.style = Paint.Style.FILL
        canvas.drawCircle((width / 2), height / 2, 12F, paint!!)
    }

    private fun drawHand(canvas: Canvas, loc: Float, isHour: Boolean) {
        val angle = Math.PI * loc / 30 - Math.PI / 2
        val handRadius = if (isHour) radius - handTruncation - hourHandTruncation else radius - handTruncation
        canvas.drawLine(
            (width / 2), (height / 2),
            (width / 2 + Math.cos(angle) * handRadius).toFloat(),
            (height / 2 + Math.sin(angle) * handRadius).toFloat(),
            paint!!
        )
    }

    private fun drawNumeral(canvas: Canvas) {
        paint!!.textSize = fontSize.toFloat()

        for (number in numbers) {
            val tmp = number.toString()
            paint!!.getTextBounds(tmp, 0, tmp.length, rect)
            val angle = Math.PI / 6 * (number - 3)
            val x = (width / 2 + Math.cos(angle) * radius - rect.width() / 2).toFloat()
            val y = (height / 2 + Math.sin(angle) * radius + rect.height() / 2).toFloat()
            canvas.drawText(tmp, x, y, paint!!)
        }
    }

    private fun drawHands(canvas: Canvas) {
        val timeZone = DateTimeZone.forID(timeZoneName)
        val dateTime = DateTime(System.currentTimeMillis(), timeZone)

        val hours = dateTime.hourOfDay().get().toFloat()
        val minutes = dateTime.minuteOfHour().get().toFloat()
        val seconds = dateTime.secondOfMinute().get().toFloat()

        drawHand(canvas, seconds, false)
        paint!!.strokeWidth = 15F
        drawHand(canvas, (hours + (minutes / 60f)) * 5f, true)
        drawHand(canvas, minutes, false)
    }



}