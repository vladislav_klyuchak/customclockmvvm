package com.vkluchak.timezonemvvm.presentation.main.view

import android.content.Context
import android.widget.ArrayAdapter
import androidx.annotation.LayoutRes

class CustomSpinnerArrayAdapter(viewContext: Context, @LayoutRes private val layoutId : Int, private var stringList: List<String>)
    : ArrayAdapter<String>(viewContext, layoutId, stringList) {

    public fun setData(stringList: List<String>){
        this.stringList = stringList
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): String? {
        return stringList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return stringList.size
    }
}