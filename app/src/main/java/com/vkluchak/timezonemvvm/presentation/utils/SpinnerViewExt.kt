package com.vkluchak.timezonemvvm.presentation.utils

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner

/**
 * set spinner onItemSelectedListener listener
 */
fun Spinner.setSpinnerItemSelectedListener(listener: ItemSelectedListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                listener.onItemSelected(parent.getItemAtPosition(position) as String)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

interface ItemSelectedListener {
    fun onItemSelected(item: String)
}
