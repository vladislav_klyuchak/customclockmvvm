package com.vkluchak.timezonemvvm.presentation.main.view

import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.vkluchak.timezonemvvm.R
import com.vkluchak.timezonemvvm.databinding.ActivityMainBinding
import com.vkluchak.timezonemvvm.infrastracture.ClockMvvmApp
import com.vkluchak.timezonemvvm.presentation.main.model.MainViewModel
import com.vkluchak.timezonemvvm.presentation.utils.ItemSelectedListener
import com.vkluchak.timezonemvvm.presentation.utils.setSpinnerItemSelectedListener
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var model: MainViewModel

    private lateinit var binding: ActivityMainBinding

    enum class ColorEnum(@ColorInt val rgb: String) {
        RED("#FF0000"),
        GREEN("#008000"),
        BLUE("#0000FF"),
        BLACK("#000000")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ClockMvvmApp.app.appComponent.inject(this)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        model.getTimeZoneList()

        val colorAdapter = CustomSpinnerArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            listOf(ColorEnum.RED.name, ColorEnum.GREEN.name, ColorEnum.BLUE.name, ColorEnum.BLACK.name))
        binding.spClockColor.adapter = colorAdapter
        binding.spClockColor.setSpinnerItemSelectedListener(object : ItemSelectedListener {
            override fun onItemSelected(item: String) {
                binding.customClockView.clockColor = Color.parseColor(enumValueOf<ColorEnum>(item).rgb)
            }
        })

        val timeZoneAdapter = CustomSpinnerArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            listOf())

        binding.spTimeZone.adapter = timeZoneAdapter
        binding.spTimeZone.setSpinnerItemSelectedListener(
            object : ItemSelectedListener {
                override fun onItemSelected(item: String) {
                    binding.customClockView.timeZoneName = item
                }
            })

        model.timeZoneListRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                timeZoneAdapter.setData(it)
            }
            .subscribe()

        model.errorReplayRelay
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext {
                Toast.makeText(this, "Error : \n $it", Toast.LENGTH_SHORT).show()
            }.subscribe()

    }
}
