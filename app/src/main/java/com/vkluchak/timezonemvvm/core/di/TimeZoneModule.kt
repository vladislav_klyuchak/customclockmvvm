package com.vkluchak.timezonemvvm.core.di

import com.vkluchak.timezonemvvm.core.TimeZoneApi
import com.vkluchak.timezonemvvm.core.TimeZoneService
import com.vkluchak.timezonemvvm.core.TimeZoneServiceImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class TimeZoneModule {

    @Provides
    internal fun provideTimeZoneService(retrofit: Retrofit): TimeZoneService =
        TimeZoneServiceImpl(retrofit.create(TimeZoneApi::class.java))
}
