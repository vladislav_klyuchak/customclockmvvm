package com.vkluchak.timezonemvvm.core

import io.reactivex.Single

interface TimeZoneService {

    fun getTimeZoneNamesList(): Single<List<String>>
}

class TimeZoneServiceImpl(private val timeZoneApi: TimeZoneApi) : TimeZoneService {
    override fun getTimeZoneNamesList() =
        timeZoneApi.getTimeZoneList()
            .map { it.timeZonesList }
            .flattenAsFlowable { it }
            .map { it.zoneName }
            .toList()!!
}