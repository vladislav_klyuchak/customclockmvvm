package com.vkluchak.timezonemvvm.core

import com.google.gson.annotations.SerializedName
import io.reactivex.Single
import retrofit2.http.GET

const val USER_KEY = "ZZRANKFNCYPF"
const val RESPONSE_FORMAT = "json"

interface TimeZoneApi {

    @GET("list-time-zone?key=$USER_KEY&format=$RESPONSE_FORMAT")
    fun getTimeZoneList(): Single<TimeZoneResponse>
}

data class TimeZoneResponse(
    @SerializedName("status") val responseStatus: String,
    @SerializedName("message") val responseMessage: String,
    @SerializedName("zones") val timeZonesList: List<TimeZone>
)

data class TimeZone(
    val countryCode: String,
    val countryName: String,
    val gmtOffset: Long,
    val timestamp: Long,
    val zoneName: String
)


