package com.vkluchak.timezonemvvm.infrastracture.di

import com.vkluchak.timezonemvvm.core.di.TimeZoneModule
import com.vkluchak.timezonemvvm.infrastracture.ClockMvvmApp
import com.vkluchak.timezonemvvm.infrastracture.network.NetworkModule
import com.vkluchak.timezonemvvm.presentation.main.view.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class, NetworkModule::class, TimeZoneModule::class]
)
interface ApplicationComponent {

    fun inject(mvvmApp: ClockMvvmApp)
    fun inject(mainActivity: MainActivity)

}