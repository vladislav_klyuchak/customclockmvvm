package com.vkluchak.timezonemvvm.infrastracture.di

import android.app.Application
import android.content.Context
import com.vkluchak.timezonemvvm.core.TimeZoneService
import com.vkluchak.timezonemvvm.presentation.main.model.MainViewModel
import com.vkluchak.timezonemvvm.presentation.main.model.MainViewModelImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

private const val PREFS = "com.vkluchak.timezonemvvm"

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideContext(): Context = application

    @Provides
    @Singleton
    internal fun providePreferences() = application.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    @Provides
    @Singleton
    internal fun provideMainViewModel(timeZoneService: TimeZoneService): MainViewModel {
        return MainViewModelImpl(timeZoneService)
    }
}