package com.vkluchak.timezonemvvm.infrastracture.network

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    companion object {
        private const val BASE_URL = "http://api.timezonedb.com/v2.1/"
        private const val READ_TIMEOUT = 10L
        private const val CONNECT_TIMEOUT = 10L
        private const val CACHE_SIZE = 10 * 1024L * 1024L
    }

    @Provides
    internal fun configureRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())

        return builder.build()
    }

    @Provides
    internal fun getBaseHttpClientBuilder(context: Context): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir,
                CACHE_SIZE
            ))

        val loggingInterceptor = HttpLoggingInterceptor()

        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY//HEADERS//BODY

        httpClient.networkInterceptors().add(StethoInterceptor())
        httpClient.addNetworkInterceptor(loggingInterceptor)

        return httpClient.build()
    }
}