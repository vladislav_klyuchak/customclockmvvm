package com.vkluchak.timezonemvvm.infrastracture

import android.app.Application
import android.util.Log.w
import com.facebook.stetho.Stetho
import com.vkluchak.timezonemvvm.infrastracture.di.ApplicationComponent
import com.vkluchak.timezonemvvm.infrastracture.di.ApplicationModule
import com.vkluchak.timezonemvvm.infrastracture.di.DaggerApplicationComponent
import io.reactivex.plugins.RxJavaPlugins
import java.io.IOException
import java.net.SocketException
import io.reactivex.functions.Consumer

class ClockMvvmApp : Application() {
    lateinit var appComponent: ApplicationComponent

    companion object {
        lateinit var app: ClockMvvmApp
    }

    override fun onCreate() {
        super.onCreate()

        app = this

        appComponent = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        Stetho.initializeWithDefaults(this)

        initRxErrorHandler()
    }

    private fun initRxErrorHandler(){
        RxJavaPlugins.setErrorHandler(Consumer { e ->
            var e = e

            /*if (e is UndeliverableException) {
            e = e.cause!!
        }*/
            if (e is SocketException || e is IOException) {
                // fine, irrelevant network problem or API that throws on cancellation
                return@Consumer
            }
            if (e is InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                return@Consumer
            }
            if (e is NullPointerException || e is IllegalArgumentException) {
                // that's likely a bug in the application
                Thread.currentThread().uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), e)
                return@Consumer
            }
            if (e is IllegalStateException) {
                // that's a bug in RxJava or in a custom operator
                Thread.currentThread().uncaughtExceptionHandler
                    .uncaughtException(Thread.currentThread(), e)
                return@Consumer
            }
            w("Undeliverable exception", e)
        })
    }

}