package com.vkluchak.timezonemvvm.infrastracture

import androidx.lifecycle.*
import io.reactivex.disposables.CompositeDisposable

abstract class ArchCompViewModel : ViewModel(), LifecycleObserver {

    private val lifecycleDisposable = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    internal fun dispatchAttach(lifecycleOwner: LifecycleOwner) {
    }

    /**
     * Called when associated view is fully initialized
     * and can be used by the model
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
    }

    /**
     * Called when corresponding activity or fragment calls #onResume()
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
    }

    /**
     * Called when corresponding activity or fragment calls #onPause()
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    internal fun dispatchDetach() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    internal fun dispatchDestroy() {

    }
}

